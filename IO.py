import data
import string

#######INPUT#######
def addInput(sent, inputList = data.inputList):
	inputList.append(sent)

def removeInput(sent, inputList = data.inputList):
	if (sent in data.inputList):
		inputList.remove(sent)

def addIgnored(word, ignoredList = data.ignoredList):
	ignoredList.append(word)
	
def removeIgnored(word, ignoredList = data.ignoredList):
	if (word in ignoredList):
		ignoredList.remove(word)
		
def addInputFrmFile(filename):
	f = file(filename)
	for line in f:
		addInput(line.strip())
	f.close()
		
def addIgnoredFrmFile(filename):
	f = file(filename)
	for line in f:
		addIgnored(line.strip())
	f.close()

######OUTPUT#######
def printAns(ansList = data.ansList):
	for item in ansList:
		chopped = item.lower().split()
		chopped[0] = chopped[0].upper()
		print ' '.join(chopped)
	
def printInput(inputList = data.inputList):
	print inputList
	
def printIgnored(ignoredList = data.ignoredList):
	print ignoredList
	
def printPerm(permList = data.permList):
	print permList