import IO
import processing
import sys

def showSel():
	print '\n\nWHAT WOULD YOU LIKE TO DO: \n'
	print '1. Add more titles '
	print '2. Add more ignored word '
	print '3. Add titles from file '
	print '4. Add ignored words from file '
	print '5. Remove a title '
	print '6. Remove an ignored word / Unignore a word'
	print '7. Show current titles '
	print '8. Show current ignored words '
	print '9. Print KWIC result '
	print '0. Exit'
	
def takeAndProcessSel():
	sel = raw_input()
	menuchoices = {'1':IO.addInput, '2':IO.addIgnored, '3':IO.addInputFrmFile, '4':IO.addIgnoredFrmFile, \
	'5':IO.removeInput, '6':IO.removeIgnored, '7':IO.printInput, '8':IO.printIgnored, '9':IO.printAns, '0':sys.exit}
	if sel not in menuchoices.keys():
		print "Invalid Choice :(\n"
	elif sel in ['1','2','3','4','5','6']:
		menuchoices[sel](raw_input())
	elif sel in ['9']:
		processing.cycle()
		processing.ignore()
		processing.sortAns()
		menuchoices[sel]()
	else:
		menuchoices[sel]()
	
def start():
	while True:
		showSel()
		takeAndProcessSel()