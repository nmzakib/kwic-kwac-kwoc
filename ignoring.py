import data
import string

def ignore(ignoredList = data.ignoredList, permList = data.permList, ansList = data.ansList):
	for word in ignoredList:
		for sent in permList:
			if not sent.startswith(word):
				ansList.append(sent)
				
	