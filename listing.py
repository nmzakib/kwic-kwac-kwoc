import data
import string

def cycle(inputList = data.inputList, permList = data.permList):
	for item in inputList:
		chopped = item.split()
		for i in range(0,len(chopped)):
			chopped.insert(0,chopped.pop())
			permList.append(' '.join(chopped))
			