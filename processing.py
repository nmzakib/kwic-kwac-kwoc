import data
import string

def cycle(inputList = data.inputList, permList = data.permList):
	#TAKES IN INPUTLIST AND PERMUTATES THEM
	#AND STORES THEM IN PERMLIST
	for item in inputList:
		chopped = item.split()
		for i in range(0,len(chopped)):
			chopped.insert(0,chopped.pop())	#cycle 
			perm = ' '.join(chopped)
			if perm not in permList:
				permList.append(perm)
				
				
def ignore(ignoredList = data.ignoredList, permList = data.permList, ansList = data.ansList):
	#COPY ANSLIST FROM PERMLIST
	for item in permList:
		if item not in ansList:
			ansList.append(item)
	
	#REMOVE UNWANTED STRINGS FROM ANSLIST
	for i in range(0,len(ignoredList)):
		for j in range(0,len(permList)):
			if (permList[j].lower()).startswith(ignoredList[i].lower()) and (permList[j] in ansList):
				ansList.remove(permList[j])
	
def sort(anyList):
	anyList = anyList.sort()
	
def sortAns():
	sort(data.ansList)